/* Le Duytam Ly
 * 1734119*/

package lab2.eclipse;

public class Bicycle {
	private String manufacturer;
	private int numberGears;
	private double maxSpeed;
	
	/**
	 * Parameterized Constructor
	 * @param manufacturer manufacturer name 
	 * @param numberGears number of gears
	 * @param maxSpeed maximum speed
	 */
	public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
		this.manufacturer = manufacturer;
		this.numberGears = numberGears;
		this.maxSpeed = maxSpeed;
	}
	
	/**
	 * Overriding the toString method
	 */
	public String toString() {
		return ("Manufacturer: "+manufacturer+", Number of Gears: "+numberGears+", MaxSpeed: "+maxSpeed);
	}
	
	/**
	 * Getter for manufacturer
	 * @return The manufacturer name 
	 */
	public String getManufacturer() {
		return manufacturer;
	}
	
	/**
	 * Getter for numberGears
	 * @return The number of gears of the bike
	 */
	public int getNumberGears() {
		return numberGears;
	}
	
	/**
	 * Getter for maxSpped
	 * @return The maximum speed of the bike
	 */
	public double getMaxSpeed() {
		return maxSpeed;
	}
}
