/* Le Duytam Ly
 * 1734119*/

package lab2.eclipse;

public class BikeStore {
	public static void main(String[] args) {
		Bicycle[] bikes = new Bicycle[4];
		/*Creating 4 bikes and putting them into the array*/
		bikes[0] = new Bicycle("Norco", 12, 30);
		bikes[1] = new Bicycle("T-Lab", 21, 50);
		bikes[2] = new Bicycle("Devinci", 24, 54);
		bikes[3] = new Bicycle("True North", 30, 60);
		
		printBikes(bikes);
		
	}
	
	/**
	 * Prints all bikes in the array
	 * @param bikes The bike array
	 */
	public static void printBikes(Bicycle[] bikes) {
		for (int i = 0 ; i < bikes.length ; i++) {
			System.out.println(bikes[i]);
		}
	}
}
